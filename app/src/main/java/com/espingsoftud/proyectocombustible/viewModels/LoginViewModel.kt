package com.espingsoftud.proyectocombustible.viewModels

import androidx.lifecycle.ViewModel

class LoginViewModel: ViewModel() {

    var listener: Listener? = null

    fun clickBtnRegistro(){
        listener?.onClickBtnRegistro()
    }

    fun clickBtnIngresar(){
        listener?.onClickBtnIngresar()
    }

    interface Listener {

        fun onClickBtnRegistro()

        fun onClickBtnIngresar()
    }
}