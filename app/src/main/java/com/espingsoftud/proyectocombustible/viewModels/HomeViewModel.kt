package com.espingsoftud.proyectocombustible.viewModels

import androidx.lifecycle.ViewModel

class HomeViewModel: ViewModel() {

    var listener: Listener? = null

    fun clickHojaDeVidaMoto(){
        listener?.onClickHojaDeVidaMoto()
    }

    fun clickIniciarRuta(){
        listener?.onClickIniciarRuta()
    }

    fun clickRegistrarRepostaje(){
        listener?.onClickRegistrarRepostaje()
    }

    fun clickCerrarSesion(){
        listener?.onClickCerrarSesion()
    }


    interface Listener{
        fun onClickHojaDeVidaMoto()
        fun onClickIniciarRuta()
        fun onClickRegistrarRepostaje()
        fun onClickCerrarSesion()
    }

}