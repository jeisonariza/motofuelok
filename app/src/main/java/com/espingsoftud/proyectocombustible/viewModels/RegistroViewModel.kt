package com.espingsoftud.proyectocombustible.viewModels

import android.os.Handler
import android.util.Log
import android.view.View
import androidx.databinding.Observable
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.espingsoftud.proyectocombustible.R
import com.espingsoftud.proyectocombustible.ktx.isInvalidEmail
import com.espingsoftud.proyectocombustible.ktx.isValidEmail

class RegistroViewModel: ViewModel() {

    var listener: Listener? = null

    fun continuarRegistro(){
        listener?.continuarRegistro()
    }

    interface Listener {

        fun continuarRegistro()
    }

}