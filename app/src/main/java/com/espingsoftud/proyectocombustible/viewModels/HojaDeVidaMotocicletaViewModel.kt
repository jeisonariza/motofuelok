package com.espingsoftud.proyectocombustible.viewModels

import androidx.lifecycle.ViewModel

class HojaDeVidaMotocicletaViewModel: ViewModel() {

    var listener: Listener? = null

    fun clickBtnRegistrarHv(){
        listener?.onClickBtnRegistrarHv()
    }

    interface Listener {

        fun onClickBtnRegistrarHv()
    }

}