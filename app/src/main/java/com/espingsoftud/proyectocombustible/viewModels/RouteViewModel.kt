package com.espingsoftud.proyectocombustible.viewModels

import androidx.lifecycle.ViewModel

class RouteViewModel: ViewModel() {

    var listener: Listener? = null

    fun clickBtnPlay(){
        listener?.onClickBtnPlay()
    }

    fun clickBtnStop(){
        listener?.onClickBtnStop()
    }

    interface Listener{

        fun onClickBtnPlay()

        fun onClickBtnStop()

    }


}