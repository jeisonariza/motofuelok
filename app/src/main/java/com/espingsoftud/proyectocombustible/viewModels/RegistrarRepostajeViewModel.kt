package com.espingsoftud.proyectocombustible.viewModels

import androidx.lifecycle.ViewModel

class RegistrarRepostajeViewModel:ViewModel() {

    var listener: Listener? = null

    fun clickRegistrarRepostaje(){
        listener?.onClickRegistarRepostaje()
    }

    interface Listener{

        fun onClickRegistarRepostaje()

    }
}