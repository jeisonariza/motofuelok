package com.espingsoftud.proyectocombustible.activities

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.espingsoftud.proyectocombustible.R
import com.espingsoftud.proyectocombustible.databinding.ActivityRegistrarRepostajeBinding
import com.espingsoftud.proyectocombustible.viewModels.RegistrarRepostajeViewModel

class RegistrarRepostaje : AppCompatActivity(), RegistrarRepostajeViewModel.Listener {

    companion object{
        fun startActivity(context: Context){
            val intent = Intent(context, RegistrarRepostaje::class.java)
            context.startActivity(intent)
        }
    }

    lateinit var binding: ActivityRegistrarRepostajeBinding

    private val viewModel: RegistrarRepostajeViewModel
    get() = ViewModelProviders.of(this).get(RegistrarRepostajeViewModel::class.java)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_registrar_repostaje)

        viewModel.listener = this
        binding.viewModel = viewModel
    }

    override fun onClickRegistarRepostaje() {

    }
}