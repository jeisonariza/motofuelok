package com.espingsoftud.proyectocombustible.activities

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.espingsoftud.proyectocombustible.R
import com.espingsoftud.proyectocombustible.databinding.ActivityHomeBinding
import com.espingsoftud.proyectocombustible.databinding.ActivityRouteBinding
import com.espingsoftud.proyectocombustible.viewModels.HomeViewModel
import com.google.firebase.auth.FirebaseAuth

class Home : AppCompatActivity(), HomeViewModel.Listener {

    companion object{
        fun startActivity(context: Context, email: String){
            val intent = Intent(context, Home::class.java)
            intent.putExtra("email", email)
            context.startActivity(intent)
        }
    }

    lateinit var binding: ActivityHomeBinding

    private val viewModel: HomeViewModel
    get() = ViewModelProviders.of(this).get(HomeViewModel::class.java)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_home)

        viewModel.listener = this
        binding.viewModel = viewModel
        Log.d("userEmail  ", intent.getStringExtra("email"))

        val preferences = getSharedPreferences(getString(R.string.preferences_file), Context.MODE_PRIVATE).edit()
        preferences.putString("email", intent.getStringExtra("email"))
        preferences.apply()
    }

    override fun onClickHojaDeVidaMoto() {

        HojaDeVidaMotocicleta.startActivity(this)
    }

    override fun onClickIniciarRuta() {

        Route.startActivity(this)
    }

    override fun onClickRegistrarRepostaje() {

        RegistrarRepostaje.startActivity(this)
    }

    override fun onClickCerrarSesion() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle(R.string.cerrar_sesion_title)
        builder.setMessage(R.string.cerrar_sesion_message)
        builder.setPositiveButton(R.string.aceptar){dialog, which ->
            val preferences = getSharedPreferences(getString(R.string.preferences_file), Context.MODE_PRIVATE).edit()
            preferences.clear()
            preferences.apply()
            FirebaseAuth.getInstance().signOut()
            onBackPressed()
            dialog.cancel()
        }
        builder.setNegativeButton(R.string.cancelar){dialog, which ->
            dialog.cancel()
        }
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }
}
