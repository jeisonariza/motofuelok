package com.espingsoftud.proyectocombustible.activities

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.espingsoftud.proyectocombustible.R
import com.espingsoftud.proyectocombustible.databinding.ActivityRegistroBinding
import com.espingsoftud.proyectocombustible.ktx.isInvalidEmail
import com.espingsoftud.proyectocombustible.viewModels.RegistroViewModel
import com.google.firebase.auth.FirebaseAuth

class Registro : AppCompatActivity(), RegistroViewModel.Listener {

    companion object{
        fun startActivity(context: Context){
            val intent = Intent(context, Registro::class.java)
            context.startActivity(intent)
        }
    }

    lateinit var binding: ActivityRegistroBinding

    private val viewModel: RegistroViewModel
    get() = ViewModelProviders.of(this).get(RegistroViewModel::class.java)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_registro)

        binding.viewModel = viewModel
        viewModel.listener = this
    }

    override fun continuarRegistro() {
        if (checkValues()){
            FirebaseAuth.getInstance()
                .createUserWithEmailAndPassword(binding.editTextCorreo.text.toString(), binding.editTextContrasena.text.toString()).addOnCompleteListener {
                    if(it.isSuccessful){
                        registerSuccess(R.string.registro_exitoso)
                    }else{
                        onError(R.string.registro_error_firebase)
                    }
                }
        }
    }

    fun checkValues(): Boolean {
        var checkValue = true
        var checkValues = 0
        if(binding.editTextCorreo.text.toString().isInvalidEmail){
            checkValues++
            onError(R.string.registro_error_email_vacio)
        }
        if (binding.editTextContrasena.text.toString().isEmpty()){
            checkValues++
            onError(R.string.registro_error_contraseña_vacia)
        }
        if (binding.editTextConfContrasena.text.toString().isEmpty()){
            checkValues++
            onError(R.string.registro_error_contraseña_confirmar)
        }
        if (binding.editTextContrasena.text.toString() != binding.editTextConfContrasena.text.toString()){
            checkValues++
            onError(R.string.registro_error_contraseña_coincidir)
        }
        if (binding.editTextNombre.text.toString().isEmpty()){
            checkValues++
            (R.string.registro_error_nombre_vacio)
        }
        if (binding.editTextPeso.text.toString().isEmpty()){
            checkValues++
            onError(R.string.registro_error_peso_vacio)
        }
        if (binding.editTextEdad.text.toString().isEmpty()){
            checkValues++
            onError(R.string.registro_error_edad_vacio)
        }
        if(checkValues >= 1){
            checkValue = false
        }
        return checkValue
    }

    fun onError(error: Int) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Error en el registro")
        builder.setMessage(error)
        builder.setPositiveButton("Aceptar"){dialog, which ->
            dialog.cancel()
        }
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }

    fun registerSuccess(mensaje: Int) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Registro Exitoso")
        builder.setMessage(mensaje)
        builder.setPositiveButton("Aceptar"){dialog, which ->
            dialog.cancel()
            Login.startActivity(this)
        }
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }

    override fun onDestroy() {
        super.onDestroy()
    }
}
