package com.espingsoftud.proyectocombustible.activities

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.espingsoftud.proyectocombustible.R
import com.espingsoftud.proyectocombustible.databinding.ActivityLoginBinding
import com.espingsoftud.proyectocombustible.viewModels.LoginViewModel
import com.google.firebase.auth.FirebaseAuth

class Login : AppCompatActivity(), LoginViewModel.Listener {

    companion object{
        fun startActivity(context: Context){
            val intent = Intent(context, Login::class.java)
            context.startActivity(intent)
        }
    }

    lateinit var binding: ActivityLoginBinding

    private val viewModel: LoginViewModel
    get() = ViewModelProviders.of(this).get(LoginViewModel::class.java)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login)

        viewModel.listener = this
        binding.viewModel = viewModel
        verifySession()
    }

    override fun onClickBtnRegistro() {
        Registro.startActivity(this)
    }

    override fun onClickBtnIngresar() {
        if(binding.editTextUsername.text.isNotEmpty() && binding.editTextPassword.text.isNotEmpty()){
            FirebaseAuth.getInstance()
                .signInWithEmailAndPassword(binding.editTextUsername.text.toString(), binding.editTextPassword.text.toString()).addOnCompleteListener {
                    if(it.isSuccessful){
                        Home.startActivity(this, it.result?.user?.email?: "")
                        finish()
                    }else{
                        onError(R.string.login_error_firebase)
                    }
                }
        }else{
            onError(R.string.login_error_vacio)
        }
    }

    fun onError(error: Int) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Error de inicio de sesion")
        builder.setMessage(error)
        builder.setPositiveButton("Aceptar"){dialog, which ->
            dialog.cancel()
        }
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }

    fun verifySession(){
        val preferences = getSharedPreferences(getString(R.string.preferences_file), Context.MODE_PRIVATE)
        val email = preferences.getString("email", null)

        if(email != null){
            Home.startActivity(this, email)
        }
    }
}
