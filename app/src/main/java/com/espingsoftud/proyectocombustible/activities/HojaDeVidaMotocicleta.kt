package com.espingsoftud.proyectocombustible.activities

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.espingsoftud.proyectocombustible.R
import com.espingsoftud.proyectocombustible.databinding.ActivityHojaDeVidaMotocicletaBinding
import com.espingsoftud.proyectocombustible.viewModels.HojaDeVidaMotocicletaViewModel

class HojaDeVidaMotocicleta : AppCompatActivity(), HojaDeVidaMotocicletaViewModel.Listener {

    companion object{
        fun startActivity(context: Context){
            val intent = Intent(context, HojaDeVidaMotocicleta::class.java)
            context.startActivity(intent)
        }
    }

    lateinit var binding: ActivityHojaDeVidaMotocicletaBinding

    private val viewModel: HojaDeVidaMotocicletaViewModel
    get() = ViewModelProviders.of(this).get(HojaDeVidaMotocicletaViewModel::class.java)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_hoja_de_vida_motocicleta)

        viewModel.listener = this
        binding.viewModel = viewModel
    }

    override fun onClickBtnRegistrarHv() {

    }
}
