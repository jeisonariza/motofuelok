package com.espingsoftud.proyectocombustible.activities

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.espingsoftud.proyectocombustible.R
import com.espingsoftud.proyectocombustible.databinding.ActivityRouteBinding
import com.espingsoftud.proyectocombustible.viewModels.RouteViewModel

class Route : AppCompatActivity(), RouteViewModel.Listener {

    companion object{
        fun startActivity(context: Context){
            val intent = Intent(context, Route::class.java)
            context.startActivity(intent)
        }
    }

    lateinit var binding: ActivityRouteBinding

    private val viewModel: RouteViewModel
    get() = ViewModelProviders.of(this).get(RouteViewModel::class.java)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_route)

        viewModel.listener = this
        binding.viewModel = viewModel
    }

    override fun onClickBtnPlay() {

    }

    override fun onClickBtnStop() {

    }
}