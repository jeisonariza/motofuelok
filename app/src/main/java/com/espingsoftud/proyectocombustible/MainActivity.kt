package com.espingsoftud.proyectocombustible

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import androidx.core.os.HandlerCompat.postDelayed
import com.espingsoftud.proyectocombustible.activities.Login
import com.google.firebase.analytics.FirebaseAnalytics
import java.util.*

class MainActivity : AppCompatActivity() {

    private var timeStart: Long = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val analytics =  FirebaseAnalytics.getInstance(this)
        val bundle = Bundle()
        bundle.putString("message", "Integracion de Firebase Completa")
        analytics.logEvent("InitScreen", bundle)

        timeStart = Calendar.getInstance().timeInMillis
        Handler().postDelayed({
            continuar()
        }, 3000)

    }

    fun continuar(){
        Login.startActivity(this)
        finish()
    }
}
