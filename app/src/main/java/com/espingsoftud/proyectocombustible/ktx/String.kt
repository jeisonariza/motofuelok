package com.espingsoftud.proyectocombustible.ktx

import android.util.Patterns

val String.isValidEmail: Boolean
    get() = Patterns.EMAIL_ADDRESS.matcher(this).matches()

val String.isInvalidEmail: Boolean
    get() = !this.isValidEmail
